# About BuildCommander

*BuildCommander* is a [0 A.D.](https://play0ad.com) mod providing a new togglable construction panel.

# Features

* A new construction panel can be toggled in proximity of the cursor, reducing eye and wrist movements. Open the *Hotkeys* menu and search for *buildcommander* to assign the hotkey that toggles the construction panel.
* Building buttons are grouped by phase requirement (phase I, phase II, phase III) to be located more intuitively.
* The default (legacy) construction panel can be kept visible or hidden. See the *Options* menu to adjust its visibility.

# Installation

[Click here](https://gitlab.com/mentula0ad/BuildCommander/-/releases/permalink/latest/downloads/buildcommander.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/BuildCommander/-/releases/permalink/latest/downloads/buildcommander.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/BuildCommander/-/releases/permalink/latest/downloads/buildcommander.zip) | [Older Releases](https://gitlab.com/mentula0ad/BuildCommander/-/releases)
