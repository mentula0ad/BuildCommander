/**
 * This class is responsible for dealing with settings.
 */
class BuildCommanderSettings
{

    getDefault()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/options/options~BuildCommander.json");
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = option.val.toString()));
        return settings;
    }

    createDefaultSettingsIfNotExist()
    {
        const settings = this.getDefault();
        Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
        Engine.ConfigDB_SaveChanges("user");
    }

}
