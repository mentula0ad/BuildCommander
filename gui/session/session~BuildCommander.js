var g_BuildCommander;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);
    g_BuildCommander = new BuildCommander();
}});
