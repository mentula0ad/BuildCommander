class BuildCommander
{

    constructor()
    {
        this.init();

        this.toggled = false;
    }

    init()
    {
        // Save default settings if they don't exist
        const settings = new BuildCommanderSettings();
        settings.createDefaultSettingsIfNotExist();
        // Register handler
        registerEntitySelectionChangeHandler(this.hide.bind(this));
    }

    show()
    {
        // Constants, XML dependent
        const side = 38; // Side length of a square button
        const margin = 1; // Margin around a button
        const border = 6; // Border between frame and squares
        const frame = 3; // Panel frame size

        const rightPanelSize = Engine.GetGUIObjectByName("unitCommands").size;
        const windowSize = Engine.GetGUIObjectByName("cinemaOverlay").getComputedSize();
        const panelCentralSize = border*2 + side*4 + margin*3;
        const panelSideSize = border*2 + side*3 + margin*2;
        const left = mouseX - windowSize.right/2 - rightPanelSize.left - panelCentralSize/2;
        const top = mouseY - windowSize.bottom - rightPanelSize.top - panelCentralSize/2;

        let obj, size;

        // Village panel
        obj = Engine.GetGUIObjectByName("unitBuildCommanderVillagePanel");
        size = obj.size;
        size.left = left;
        size.right = left + panelCentralSize;
        size.top = top;
        size.bottom = top + panelCentralSize;
        obj.size = size;

        // Town panel
        obj = Engine.GetGUIObjectByName("unitBuildCommanderTownPanel");
        size = obj.size;
        size.left = left - panelSideSize + frame;
        size.right = left + frame;
        size.top = top + (side+margin)/2;
        size.bottom = top + panelSideSize + (side+margin)/2;
        obj.size = size;

        // City panel
        obj = Engine.GetGUIObjectByName("unitBuildCommanderCityPanel");
        size = obj.size;
        size.left = left + panelCentralSize - frame;
        size.right = left + panelSideSize + panelCentralSize - frame;
        size.top = top + (side+margin)/2;
        size.bottom = top + panelSideSize + (side+margin)/2;
        obj.size = size;

        this.toggled = true;
    }

    hide()
    {
        this.toggled = false;
    }

    toggle()
    {
        const action = Engine.ConfigDB_GetValue("user", "buildcommander.action");
        (!this.toggled || action === "1") ? this.show() : this.hide();
    }

}
