// Village object
g_SelectionPanels.BuildCommanderVillage = {
    "getMaxNumberOfItems": g_SelectionPanels.Construction.getMaxNumberOfItems,
    "rowLength": 4,
    "getItems": function()
    {
        if (!global.g_BuildCommander || !g_BuildCommander.toggled)
            return [];

        return getAllBuildableEntitiesFromSelection().filter(x => [undefined, "phase_village"].includes(GetTemplateData(x).requirements?.Techs?._string));
    },
    "setupButton": function(data)
    {
        const ret = g_SelectionPanels.Construction.setupButton(data);
        data.button.onPress = function() {
            startBuildingPlacement(data.item, data.playerState);
            g_BuildCommander.hide();
        };
        return ret;
    }
};

// Town object
g_SelectionPanels.BuildCommanderTown = {
    "getMaxNumberOfItems": g_SelectionPanels.Construction.getMaxNumberOfItems,
    "rowLength": 3,
    "getItems": function()
    {
        if (!global.g_BuildCommander || !g_BuildCommander.toggled)
            return [];

        return getAllBuildableEntitiesFromSelection().filter(x => GetTemplateData(x).requirements?.Techs?._string == "phase_town");
    },
    "setupButton": function(data)
    {
        const ret = g_SelectionPanels.Construction.setupButton(data);
        data.button.onPress = function() {
            startBuildingPlacement(data.item, data.playerState);
            g_BuildCommander.hide();
        };
        return ret;
    }
};

// City object
g_SelectionPanels.BuildCommanderCity = {
    "getMaxNumberOfItems": g_SelectionPanels.Construction.getMaxNumberOfItems,
    "rowLength": 3,
    "getItems": function()
    {
        if (!global.g_BuildCommander || !g_BuildCommander.toggled)
            return [];

	return getAllBuildableEntitiesFromSelection().filter(x => GetTemplateData(x).requirements?.Techs?._string == "phase_city");
    },
    "setupButton": function(data)
    {
        const ret = g_SelectionPanels.Construction.setupButton(data);
        data.button.onPress = function() {
            startBuildingPlacement(data.item, data.playerState);
            g_BuildCommander.hide();
        };
        return ret;
    }
};

g_SelectionPanels.Construction.getItems = new Proxy(g_SelectionPanels.Construction.getItems, {apply: function(target, thisArg, args) {
    const legacy = Engine.ConfigDB_GetValue("user", "buildcommander.legacy");
    if (legacy === "2")
        return [];
    if (legacy === "1" && !!global.g_BuildCommander && g_BuildCommander.toggled)
        return [];
    return target(...args);
}});

// Push new panels
g_PanelsOrder.push("BuildCommanderVillage");
g_PanelsOrder.push("BuildCommanderTown");
g_PanelsOrder.push("BuildCommanderCity");
