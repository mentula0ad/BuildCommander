getNumberOfRightPanelButtons = new Proxy(getNumberOfRightPanelButtons, {apply: function(target, thisArg, args) {
    if (!!global.g_BuildCommander && g_BuildCommander.toggled)
        return 0;
    return target(...args);
}});
